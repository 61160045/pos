package com.watt.pos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductService {
    private static ArrayList<Product> productList = new ArrayList<>();
    
    //Add
    public static boolean addProduct(Product product) {
        productList.add(product);
        save();
        return true;
    }
    //Delete
    public static boolean delProduct(Product product) {
        productList.remove(product);
        save();
        return true;
    }
    public static boolean delProduct(int index) {
        productList.remove(index);
        save();
        return true;
    }
    //Read
    public static ArrayList<Product> getProducts() {
        return productList;
    }
    public static Product getProduct(int index) {
        return productList.get(index);
    }
    //Update
    public static boolean updateProduct(int index, Product product) {
        productList.set(index, product);
        save();
        return true;
    }
    
    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("watt.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            oos.close();
            fos.close();
        }catch (FileNotFoundException  ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("watt.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Product>)ois.readObject();
            ois.close();
            fis.close();
        }catch (FileNotFoundException  ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static String total() {
        double sum = 0.0;
        int amt = 0;
        for(int i = 0;i < productList.size();i++) {
            sum += productList.get(i).getPrice();
            amt += productList.get(i).getAmount();
        }
        return "Price : " + sum + " $ , Total_Amount : " + amt + " Qty";
    }
}
