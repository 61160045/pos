package com.watt.pos;

import java.io.Serializable;

public class Product implements Serializable{

    private int id, amount;
    private String name;
    private String brand;
    private double price;

    public Product(int id, String name, String brand, double price, int amount) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.amount = amount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public double getPrice() {
        return price;
    }

    public int getAmount() {
        return amount;
    }

    public String toString() {
        return "[id : " + id + "   productname : " + name + "   brand : " + brand + "   price : " + price + "   amount : " + amount + " ]";
    }
}
